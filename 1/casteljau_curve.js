//noinspection JSHint,JSLint
define(["point_dragger", "vec2", "straight_line", "dragger_line", "parametric_curve"],
    function (PointDragger, vec2, StraightLine, DraggerLine, ParametricCurve) {
        "use strict";

        var CasteljauCurve = function (p0, p1, p2, p3, t, level, style, tickmarks, showCasteljauLines) {
            this.points = [p0, p1, p2, p3];
            this.tickmarks = tickmarks;

            this.showCasteljauLines = showCasteljauLines;
            this.t = t;
            this.level = level;
            this.minT = 0;
            // needed because lines would be shared between all subclasses if not overwritten
            this.lines = [];
            // helper array to save the points where the casteljau indicator lines will be connected
            this.levelLinePoints = [];
            this.lineStyle = style;
            this.curves = [];
            this.updateCurves();
        };

        CasteljauCurve.prototype = new ParametricCurve();

        CasteljauCurve.prototype.toggleCasteljauLines = function (showCasteljauLines) {
            this.showCasteljauLines = showCasteljauLines;
            this.curves = [];
            this.updateCurves();
        };

        CasteljauCurve.prototype.setLevel = function (level) {
            this.level = level;
            this.curves = [];
            this.updateCurves();
        };

        /**
         * Creates all part-curves by first using the casteljau algorithm to split the
         * control points to the user defined level, then connecting them
         * by calling createPartCurve on each set of points
         */
        CasteljauCurve.prototype.updateCurves = function () {
            this.levelLinePoints = [];
            var i, curve, controllers;
            // splits up our initial points
            controllers = this.createPoints([this.points], 0);
            for(i = 0; i < controllers.length; i++) {
                curve = controllers[i];
                this.curves[i] = this.createPartCurve(curve);
            }
            this.segments = controllers.length * 3;
            this.maxT = controllers.length - 1;
            this.steps = this.maxT / this.segments;
        };

        CasteljauCurve.prototype.casteljauSplit = function (points) {
            var p = [points],
                n = points.length - 1,
                leftHalf = [p[0][0]],
                rightHalf = [],
                j, i;
            rightHalf[n] = p[0][n];
            // splits the points until there is only one point left
            for(j = 1; j <= n; j++) {
                p[j] = [];
                for(i = 0; i <= n - j; i++) {
                    //create new point with casteljau algorithm
                    p[j][i] = [(1 - this.t) * p[j - 1][i][0] + this.t * p[j - 1][i + 1][0], (1 - this.t) * p[j - 1][i][1] + this.t * p[j - 1][i + 1][1]];
                }
                // save this level to be displayed later
                this.levelLinePoints.push(p[j]);
                leftHalf[j] = p[j][0];
                rightHalf[n - j] = p[j][i - 1];
            }
            return [leftHalf, rightHalf];
        };

        CasteljauCurve.prototype.createPoints = function (curves, level) {
            if(level === this.level) {
                return curves;
            }
            var x, newCurves = [];
            for(x = 0; x < curves.length; x++) {
                newCurves = newCurves.concat(this.casteljauSplit(curves[x]));
            }
            return this.createPoints(newCurves, level + 1);
        };

        CasteljauCurve.prototype.createPartCurve = function (points) {
            return new ParametricCurve(function (t) {
                return points[t][0];
            }, function (t) {
                return points[t][1];
            }, 0, 3, 3, this.lineStyle, this.tickmarks);
        };

        CasteljauCurve.prototype.toggleTickMarks = function (state) {
            this.tickmarks = state;
            this.updateCurves();
        };

        CasteljauCurve.prototype.draw = function (context) {
            var i;
            for(i = 0; i < this.curves.length; i++) {
                this.curves[i].draw(context);
            }
            context.beginPath();
            for(i = 0; i < this.curves.length - 1; i++) {
                context.moveTo(this.curves[i].f(3), this.curves[i].g(3));
                context.lineTo(this.curves[i + 1].f(0), this.curves[i + 1].g(0));
            }
            context.lineWidth = this.lineStyle.width;
            context.strokeStyle = this.lineStyle.color;

            context.stroke();
        };

        CasteljauCurve.prototype.isHit = function (context, pos) {
            var i;
            for(i = 0; i < this.curves.length; i++) {
                if(this.curves[i].isHit(context, pos)) {
                    return true;
                }
            }
            return false;
        };

        CasteljauCurve.prototype.addPoint = function (point) {
            this.points.push(point);
            this.updateCurves();
        };

        CasteljauCurve.prototype.createDraggers = function () {
            var pointIndex, casteljauLevel, levelPoint, pointCounter, draggers = [], _self = this, draggerLines = [], casteljauLines = [],
                draggerStyle = { radius : 4, color : this.lineStyle.color, width : 0, fill : true };

            // create the "normal" draggerlines
            for(pointCounter = 0; pointCounter < this.points.length - 1; pointCounter++) {
                draggerLines.push(new DraggerLine(this.points[pointCounter], this.points[pointCounter + 1], this.lineStyle));
            }

            if(this.showCasteljauLines) {
                // create the lines for the casteljau levels
                for(casteljauLevel = 0; casteljauLevel < _self.levelLinePoints.length; casteljauLevel++) {
                    for(levelPoint = 0; levelPoint < _self.levelLinePoints[casteljauLevel].length - 1; levelPoint++) {
                        casteljauLines.push(new DraggerLine(_self.levelLinePoints[casteljauLevel][levelPoint], _self.levelLinePoints[casteljauLevel][levelPoint + 1], _self.lineStyle));
                    }
                }
            }

            // update when dragged
            function updateDraggerLines() {
                var pointCounter;
                for(pointCounter = 0; pointCounter < _self.points.length - 1; pointCounter++) {
                    draggerLines[pointCounter].p0 = _self.points[pointCounter];
                    draggerLines[pointCounter].p1 = _self.points[pointCounter + 1];
                }
            }

            function updateCasteljauLines() {
                if(_self.showCasteljauLines) {
                    var p = 0, j, x;
                    for(j = 0; j < _self.levelLinePoints.length; j++) {
                        for(x = 0; x < _self.levelLinePoints[j].length - 1; x++) {
                            casteljauLines[p].p0 = _self.levelLinePoints[j][x];
                            casteljauLines[p].p1 = _self.levelLinePoints[j][x + 1];
                            p++;
                        }
                    }
                }
            }

            function createPointDragger(index) {
                return new PointDragger(function () {
                    return _self.points[index];
                }, function (dragEvent) {
                    _self.points[index] = dragEvent.position;
                    _self.updateCurves();
                    updateCasteljauLines();
                    updateDraggerLines();
                }, draggerStyle);
            }

            // create a dragger for every point
            for(pointIndex = 0; pointIndex < this.points.length; pointIndex++) {
                draggers.push(createPointDragger(pointIndex));
            }

            return draggers.concat(draggerLines).concat(casteljauLines);
        };

        return CasteljauCurve;
    }
);