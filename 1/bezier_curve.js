//noinspection JSHint,JSLint
define(["point_dragger", "vec2", "parametric_curve", "straight_line", "dragger_line"],
    function (PointDragger, vec2, ParametricCurve, StraightLine, DraggerLine) {
        "use strict";

        var BezierCurve = function (p0, p1, p2, p3, segments, style, tickmarks) {
            this.p0 = p0;
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.segments = segments;
            this.steps = this.maxT / this.segments;
            // needed because lines would be shared between all subclasses if not overwritten
            this.lines = [];
            this.tickmarks = tickmarks;
            this.lineStyle = style;
        };

        BezierCurve.prototype = new ParametricCurve(function (t) {
            return Math.pow(1 - t, 3) * this.p0[0] + 3 * Math.pow((1 - t), 2) * t * this.p1[0] + 3 * (1 - t) * (t * t) * this.p2[0] + Math.pow(t, 3) * this.p3[0];
        }, function (t) {
            return Math.pow(1 - t, 3) * this.p0[1] + 3 * Math.pow((1 - t), 2) * t * this.p1[1] + 3 * (1 - t) * (t * t) * this.p2[1] + Math.pow(t, 3) * this.p3[1];
        }, 0, 1, 20, null);

        BezierCurve.prototype.createDraggers = function () {
            var _self = this, draggerLines, draggers,
                draggerStyle = { radius : 4, color : this.lineStyle.color, width : 0, fill : true };

            function createPointDragger(name, lines) {
                return new PointDragger(function () {
                    return _self[name];
                }, function (dragEvent) {
                    _self[name] = dragEvent.position;
                    if(lines[0]) {
                        lines[0].p0 = dragEvent.position;
                    }
                    if(lines[1]) {
                        lines[1].p1 = dragEvent.position;
                    }
                }, draggerStyle);
            }

            draggerLines = [new DraggerLine(this.p0, this.p1, this.lineStyle), new DraggerLine(this.p1, this.p2, this.lineStyle), new DraggerLine(this.p2, this.p3, this.lineStyle)];

            draggers = [
                createPointDragger("p0", [draggerLines[0]]),
                createPointDragger("p1", [draggerLines[1], draggerLines[0]]),
                createPointDragger("p2", [draggerLines[2], draggerLines[1]]),
                createPointDragger("p3", [null, draggerLines[2]])
            ];

            return draggers.concat(draggerLines);

        };
        return BezierCurve;
    }
);