//noinspection JSHint,JSLint
define(["point_dragger", "vec2"],
    function (PointDragger, vec2) {
        "use strict";

        var ParametricCurve = function (f, g, minT, maxT, segments, lineStyle, tickmarks) {
            this.f = f;
            this.g = g;
            this.minT = minT;
            this.maxT = maxT;
            this.segments = segments;
            this.steps = this.maxT / this.segments;
            this.lines = [];
            this.lineStyle = lineStyle || { width : "2", color : "#0000AA" };
            this.tickmarks = tickmarks;
        };

        ParametricCurve.prototype.toggleTickMarks = function (state) {
            this.tickmarks = state;
        };

        ParametricCurve.prototype.draw = function (context) {
            var t, x1, x0, x2, y1, y2, y0, marks1, marks2, marks3;

            this.lines = [];

            context.beginPath();

            for(t = this.minT; t < this.maxT; t += this.steps) {
                x1 = this.f(t);
                x2 = this.f(t + this.steps);
                y1 = this.g(t);
                y2 = this.g(t + this.steps);
                this.lines.push({
                    p0 : [x1, y1],
                    p1 : [x2, y2]
                });
                context.moveTo(x1, y1);
                context.lineTo(x2, y2);

                if(this.tickmarks === true && t !== 0) {
                    x0 = this.f(t - this.steps);
                    y0 = this.g(t - this.steps);
                    marks1 = vec2.sub([x0, y0], [x2, y2]);
                    marks2 = vec2.add([x1, y1], vec2.mult([-marks1[1], marks1[0]], 0.05));
                    marks3 = vec2.sub([x1, y1], vec2.mult([-marks1[1], marks1[0]], 0.05));
                    context.moveTo(x1, y1);
                    context.lineTo(marks2[0], marks2[1]);
                    context.moveTo(x1, y1);
                    context.lineTo(marks3[0], marks3[1]);
                }
            }

            context.lineWidth = this.lineStyle.width;
            context.strokeStyle = this.lineStyle.color;
            context.stroke();
        };

        ParametricCurve.prototype.isHit = function (context, pos) {
            var t, p, d, i;
            for(i = 0; i < this.lines.length; i++) {
                t = vec2.projectPointOnLine(pos, this.lines[i].p0, this.lines[i].p1);
                p = vec2.add(this.lines[i].p0, vec2.mult(vec2.sub(this.lines[i].p1, this.lines[i].p0), t));
                d = vec2.length(vec2.sub(p, pos));

                if(d <= (this.lineStyle.width / 2) + 2) {
                    return true;
                }
            }
            return false;
        };

        ParametricCurve.prototype.createDraggers = function () {
            return [];
        };

        return ParametricCurve;
    }
);