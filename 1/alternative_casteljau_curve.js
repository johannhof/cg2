//noinspection JSHint,JSLint
define(["point_dragger", "vec2", "straight_line", "dragger_line", "parametric_curve"],
    function (PointDragger, vec2, StraightLine, DraggerLine, ParametricCurve) {
        "use strict";

        var CasteljauCurve = function (p0, p1, p2, p3, t, style, tickmarks, showCasteljauLines) {
            this.points = [p0, p1, p2, p3];
            this.tickmarks = tickmarks;

            this.showCasteljauLines = showCasteljauLines;
            this.minT = 0;
            this.maxT = 1;
            this.segments = 100;
            this.steps = 0.01;
            // needed because lines would be shared between all subclasses if not overwritten
            this.lines = [];
            // helper array to save the points where the casteljau indicator lines will be connected
            this.showLevelLinesAt = t;
            this.levelLinePoints = [];
            this.lineStyle = style;
        };

        CasteljauCurve.prototype = new ParametricCurve(function (t) {
            return this.casteljau(t)[0];
        }, function (t) {
            return this.casteljau(t)[1];
        });

        CasteljauCurve.prototype.toggleCasteljauLines = function (showCasteljauLines) {
            this.showCasteljauLines = showCasteljauLines;
        };

        CasteljauCurve.prototype.setT = function (t) {
            this.showLevelLinesAt = t;
        };

        CasteljauCurve.prototype.casteljau = function (t) {
            var p = [this.points],
                n = this.points.length - 1,
                j, i;
            // splits the points until there is only one point left
            for(j = 1; j <= n; j++) {
                p[j] = [];
                for(i = 0; i <= n - j; i++) {
                    //create new point with casteljau algorithm
                    p[j][i] = [(1 - t) * p[j - 1][i][0] + t * p[j - 1][i + 1][0], (1 - t) * p[j - 1][i][1] + t * p[j - 1][i + 1][1]];
                }
            }
            if(this.showLevelLinesAt * 100 === Math.round(t * 100)) {
                // save this level to be displayed later
                this.levelLinePoints = p;
            }
            return p[n][0];
        };


        CasteljauCurve.prototype.toggleTickMarks = function (state) {
            this.tickmarks = state;
        };

        CasteljauCurve.prototype.addPoint = function (point) {
            this.points.push(point);
        };

        CasteljauCurve.prototype.createDraggers = function () {
            var pointIndex, casteljauLevel, levelPoint, pointCounter, draggers = [], _self = this, draggerLines = [], casteljauLines = [],
                draggerStyle = { radius : 4, color : this.lineStyle.color, width : 0, fill : true };

            // create the "normal" draggerlines
            for(pointCounter = 0; pointCounter < this.points.length - 1; pointCounter++) {
                draggerLines.push(new DraggerLine(this.points[pointCounter], this.points[pointCounter + 1], this.lineStyle));
            }

            if(this.showCasteljauLines) {
                // create the lines for the casteljau levels
                for(casteljauLevel = 0; casteljauLevel < _self.levelLinePoints.length; casteljauLevel++) {
                    for(levelPoint = 0; levelPoint < _self.levelLinePoints[casteljauLevel].length - 1; levelPoint++) {
                        casteljauLines.push(new DraggerLine(_self.levelLinePoints[casteljauLevel][levelPoint], _self.levelLinePoints[casteljauLevel][levelPoint + 1], _self.lineStyle));
                    }
                }
            }

            // update when dragged
            function updateDraggerLines() {
                var pointCounter;
                for(pointCounter = 0; pointCounter < _self.points.length - 1; pointCounter++) {
                    draggerLines[pointCounter].p0 = _self.points[pointCounter];
                    draggerLines[pointCounter].p1 = _self.points[pointCounter + 1];
                }
            }

            function updateCasteljauLines() {
                if(_self.showCasteljauLines) {
                    var p = 0, j, x;
                    for(j = 0; j < _self.levelLinePoints.length; j++) {
                        for(x = 0; x < _self.levelLinePoints[j].length - 1; x++) {
                            casteljauLines[p].p0 = _self.levelLinePoints[j][x];
                            casteljauLines[p].p1 = _self.levelLinePoints[j][x + 1];
                            p++;
                        }
                    }
                }
            }

            function createPointDragger(index) {
                return new PointDragger(function () {
                        return _self.points[index];
                    }, function (dragEvent) {
                        _self.points[index] = dragEvent.position;
                        updateCasteljauLines();
                        updateDraggerLines();
                    }, draggerStyle
                );
            }

            // create a dragger for every point
            for(pointIndex = 0; pointIndex < this.points.length; pointIndex++) {
                draggers.push(createPointDragger(pointIndex));
            }

            return draggers.concat(draggerLines).concat(casteljauLines);
        };

        return CasteljauCurve;
    }
);