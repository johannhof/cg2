/*
 * JavaScript / Canvas teaching framwork 
 * (C)opyright Hartmut Schirmacher, hschirmacher.beuth-hochschule.de 
 *
 * Module: html_controller
 *
 * Defines callback functions for communicating with various 
 * HTML elements on the page, e.g. buttons and parameter fields.
 *
 */

/* requireJS module definition */
//noinspection JSHint,JSLint
define(["jquery", "straight_line", "circle", "parametric_curve", "bezier_curve", "casteljau_curve", "alternative_casteljau_curve"],
    function ($, StraightLine, Circle, ParametricCurve, BezierCurve, CasteljauCurve, AlternativeCasteljauCurve) {

        "use strict";

        /*
         * define callback functions to react to changes in the HTML page
         * and provide them with a closure defining context and scene
         */
        return function (context, scene, sceneController) {
            var randomX, randomY, randomColor;

            // generate random X coordinate within the canvas
            randomX = function () {
                return Math.floor(Math.random() * (context.canvas.width - 10)) + 5;
            };

            // generate random Y coordinate within the canvas
            randomY = function () {
                return Math.floor(Math.random() * (context.canvas.height - 10)) + 5;
            };

            // generate random color in hex notation
            randomColor = function () {
                var toHex2, r, g, b;

                // convert a byte (0...255) to a 2-digit hex string
                toHex2 = function (byteVar) {
                    var s = byteVar.toString(16); // convert to hex string
                    if(s.length === 1) {
                        s = "0" + s;
                    } // pad with leading 0
                    return s;
                };

                r = Math.floor(Math.random() * 25.9) * 10;
                g = Math.floor(Math.random() * 25.9) * 10;
                b = Math.floor(Math.random() * 25.9) * 10;

                // convert to hex notation
                return "#" + toHex2(r) + toHex2(g) + toHex2(b);
            };

            function showInputElements(obj) {
                if(!obj) {
                    return;
                }
                var radiusInput = $("#radiusInput"),
                    tickmarks = $("#tickSpan"),
                    paraCurveParams = $("#paraCurveParams"),
                    casteljauParams = $("#casteljauParams"),
                    addPointButton = $("#addPointButton"),
                    casteljauLineSpan = $("#casteljauLineSpan"),
                    tPointInputSpan = $("#tPointInputSpan");
                if(obj.radius !== undefined) {
                    radiusInput.show();
                    radiusInput.val(obj.radius);
                } else {
                    radiusInput.hide();
                }
                tPointInputSpan.hide();
                paraCurveParams.hide();
                if(obj instanceof CasteljauCurve) {
                    casteljauParams.show();
                    tickmarks.show();
                    addPointButton.show();
                    casteljauLineSpan.show();
                } else {
                    casteljauLineSpan.hide();
                    addPointButton.hide();
                    tickmarks.hide();
                    casteljauParams.hide();
                    if(obj instanceof AlternativeCasteljauCurve) {
                        tPointInputSpan.show();
                        addPointButton.show();
                        casteljauLineSpan.show();
                        tickmarks.show();
                    } else if(obj instanceof BezierCurve) {
                        tickmarks.show();
                    } else {
                        if(obj instanceof ParametricCurve) {
                            tickmarks.show();
                            paraCurveParams.show();
                        } else {
                            paraCurveParams.hide();
                        }
                    }
                }
            }

            $("#elementSelection").change(function () {
                var obj;

                switch($(this).val()) {
                    case "line":
                        obj = new StraightLine([0, 0], [0, 0]);
                        break;
                    case "circle":
                        obj = new Circle([0, 0], 0, {});
                        break;
                    case "paraCurve":
                        obj = new ParametricCurve({}, {}, 0, 1, 1, {}, false);
                        break;
                    case "bezierCurve":
                        obj = new BezierCurve([0, 0], [0, 0], [0, 0], [0, 0], 10, {}, false);
                        break;
                    case "casteljauCurve":
                        obj = new CasteljauCurve([0, 0], [0, 0], [0, 0], [0, 0], 1, 1, {}, false);
                        break;
                    case "casteljauCurveAlt":
                        obj = new AlternativeCasteljauCurve([0, 0], [0, 0], [0, 0], [0, 0], 1, {}, false);
                        break;
                    default:
                        return;
                }
                showInputElements(obj);
            });

            sceneController.onSelection(function (obj) {
                    $("#LineWidthInput").val(obj.lineStyle.width);
                    $("#ColorInput").val(obj.lineStyle.color);
                    showInputElements(obj);
                }
            );

            sceneController.onObjChange(showInputElements);

            $("#radiusInput").change(function () {
                    var obj = sceneController.getSelectedObject();
                    if(obj && obj.radius) {
                        obj.radius = parseInt($(this).val() || obj.radius, 10);
                    }
                    scene.draw(context);
                }
            );

            $("#LineWidthInput").change(function () {
                    sceneController.getSelectedObject().lineStyle.width = $(this).val();
                    scene.draw(context);
                }
            );

            function reselect(selected) {
                scene.draw(context);
                sceneController.deselect();
                sceneController.select(selected);
            }

            $("#ColorInput").change(function () {
                    var selected = sceneController.getSelectedObject();
                    selected.lineStyle.color = $(this).val();
                    reselect(selected);
                }
            );

            $("#tickMarkCheckBox").click(function () {
                var object = sceneController.getSelectedObject();
                if(object && object.toggleTickMarks) {
                    object.toggleTickMarks(this.checked);
                    reselect(object);
                }
            });

            $("#addPointButton").click(function () {
                var object = sceneController.getSelectedObject();
                if(object && object.addPoint) {
                    object.addPoint([randomX(), randomY()]);
                    reselect(object);
                }
            });

            $("#levelInput").change(function () {
                var object = sceneController.getSelectedObject();
                if(object && object.setLevel) {
                    object.setLevel(parseInt(this.value, 10));
                    reselect(object);
                }
            });

            $("#casteljauLineCheckBox").change(function () {
                var object = sceneController.getSelectedObject();
                if(object && object.toggleCasteljauLines) {
                    object.toggleCasteljauLines(this.checked);
                    reselect(object);
                }
            });

            $("#tPointInput").change(function () {
                var object = sceneController.getSelectedObject();
                if(object && object.setT) {
                    object.setT(this.value);
                    reselect(object);
                }
            });

            $("#createElementButton").click(function () {
                var object, calc, random = Boolean($("#randomBox").attr("checked")),
                    style = {
                        width : random ? Math.floor(Math.random() * 3) + 1 : parseInt($("#LineWidthInput").val(), 10),
                        color : random ? randomColor() : $("#ColorInput").val()
                    },
                    objectType = $("#elementSelection").val();

                switch(objectType) {
                    case "line" :
                        object = new StraightLine([randomX(), randomY()], [randomX(), randomY()], style);
                        break;
                    case "circle":
                        object = new Circle([randomX(), randomY()], random ? Math.random() * 100 : parseInt($("#radiusInput").val(), 10), style);
                        break;
                    case "paraCurve":
                        try {
                            var t = parseFloat($("#minTInput").val());
                            eval($("#xInput").val());
                            eval($("#yInput").val());
                        } catch(e) {
                            window.alert("Invalid Input");
                            return;
                        }
                        calc = function (x) {
                            return function (t) {
                                try {
                                    return eval(x);
                                } catch(e) {
                                    return 0;
                                }
                            };
                        };
                        object = new ParametricCurve(calc($("#xInput").val()), calc($("#yInput").val()), parseFloat($("#minTInput").val()), parseFloat($("#maxTInput").val()), parseFloat($("#segmentsInput").val()), style, $("#tickMarkCheckBox").is(':checked'));
                        break;
                    case "bezierCurve":
                        object = new BezierCurve([randomX(), randomY()], [randomX(), randomY()], [randomX(), randomY()], [randomX(), randomY()], parseFloat($("#segmentsInput").val()), style, $("#tickMarkCheckBox").attr("checked"));
                        break;
                    case "casteljauCurve":
                        object = new CasteljauCurve([randomX(), randomY()], [randomX(), randomY()], [randomX(), randomY()], [randomX(), randomY()], parseFloat($("#tInput").val()), parseInt($("#levelInput").val(), 10), style, $("#tickMarkCheckBox").is(':checked'), $("#casteljauLineCheckBox").is(':checked'));
                        break;
                    case "casteljauCurveAlt":
                        object = new AlternativeCasteljauCurve([randomX(), randomY()], [randomX(), randomY()], [randomX(), randomY()], [randomX(), randomY()], parseFloat($("#tPointInput").val()), style, $("#tickMarkCheckBox").is(':checked'), $("#casteljauLineCheckBox").is(':checked'));
                        break;
                    default:
                        throw "Error selecting element from DOM!";
                }

                scene.addObjects([object]);

                reselect(object);
            });

            $("#removeButton").click(function () {
                scene.removeObjects([sceneController.getSelectedObject()]);
                sceneController.deselect();
                scene.draw(context);
            });

        };
    }
)
; // require