/*
 * JavaScript / Canvas teaching framwork 
 * (C)opyright Hartmut Schirmacher, hschirmacher.beuth-hochschule.de 
 *
 * Module: vec2
 *
 * Some simple 2D vector operations based on [x,y] arrays
 *
 */


/* requireJS module definition */
//noinspection JSHint,JSLint
define([],
    function () {

        "use strict";

        var vec2 = {};

        // add two vectors, return new vector
        vec2.add = function (v0, v1) {
            return [v0[0] + v1[0], v0[1] + v1[1] ];
        };

        // subtract two vectors, return new vector
        vec2.sub = function (v0, v1) {
            return [v0[0] - v1[0], v0[1] - v1[1] ];
        };

        // dot product / scalar product of two vectors, return scalar value
        vec2.dot = function (v0, v1) {
            return v0[0] * v1[0] + v0[1] * v1[1];
        };

        // multiply vector by scalar, return new vector
        vec2.mult = function (v, s) {
            return [ v[0] * s, v[1] * s ];
        };

        // return squared length of a vector
        vec2.length2 = function (v) {
            return vec2.dot(v, v);
        };

        vec2.normalize = function (v) {
            var len = vec2.length(v);
            if(len > 0) {
                v[0] = v[0] / len;
                v[1] = v[1] / len;
            } else {
                v[0] = 0;
                v[1] = 0;
            }
            return v;
        };

        /**
         * returns the squared distance between two points
         *
         * @param p0 point 1
         * @param p1 point 2
         * @returns Number the squared distance between the two
         */
        vec2.distance2 = function (p0, p1) {
            return (p0[0] - p1[0]) * (p0[0] - p1[0]) + (p0[1] - p1[1]) * (p0[1] - p1[1]);
        };

        /**
         * returns the distance between two points
         *
         * @param p0 point 1
         * @param p1 point 2
         * @returns Number the distance between the two
         */
        vec2.distance = function (p0, p1) {
            return Math.sqrt(vec2.distance2(p0, p1));
        };

        // length of a vector
        vec2.length = function (v) {
            return Math.sqrt(vec2.dot(v, v));
        };

        // project a point onto a line defined by two points.
        // return scalar parameter of where point p is projected
        // onto the line. the line segment between p0 and 01
        // corresponds to the value range [0:1]
        vec2.projectPointOnLine = function (p, p0, p1) {
            var dp, dv, t;

            dp = vec2.sub(p, p0);
            dv = vec2.sub(p1, p0);
            t = vec2.dot(dp, dv) / vec2.dot(dv, dv);
            return t;

        };

        // this module exports an object defining a number of functions
        return vec2;

    }
); // define

    
