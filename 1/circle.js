define(["point_dragger", "vec2"],
    function (PointDragger, vec2) {
        "use strict";

        var Circle = function (center, radius, lineStyle) {
            this.center = center;
            this.radius = radius;
            this.lineStyle = lineStyle || { width : "2", color : "#0000AA" };
        };

        Circle.prototype.draw = function (context) {
            context.beginPath();
            context.arc(this.center[0], this.center[1], this.radius, 0, Math.PI * 2, false);
            context.lineWidth = this.lineStyle.width;
            context.strokeStyle = this.lineStyle.color;
            context.stroke();
        };

        Circle.prototype.isHit = function (context, pos) {
            var formula = vec2.distance2(pos, this.center);
            return formula < this.radius * this.radius + 250 && formula > this.radius * this.radius - 250;
        };

        Circle.prototype.createDraggers = function () {
            var draggerStyle, _self, getP, setP, getPR, setPR;
            draggerStyle = { radius : 4, color : this.lineStyle.color, width : 0, fill : true };

            _self = this;

            getP = function () {
                return _self.center;
            };

            getPR = function () {
                return [_self.center[0] + _self.radius, _self.center[1]];
            };

            setP = function (dragEvent) {
                _self.center = dragEvent.position;
            };

            setPR = function (dragEvent) {
                _self.radius = vec2.distance(dragEvent.position, _self.center);
            };

            return [new PointDragger(getP, setP, draggerStyle), new PointDragger(getPR, setPR, draggerStyle)];

        };

        return Circle;
    }
);