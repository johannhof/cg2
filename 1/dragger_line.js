//noinspection JSHint,JSLint
define(["point_dragger", "vec2", "parametric_curve", "straight_line"],
    function (PointDragger, vec2, ParametricCurve, StraightLine) {
        "use strict";

        var DraggerLine = function (p0, p1, lineStyle) {
            this.p0 = p0 || [10, 10];
            this.p1 = p1 || [10, 10];
            this.lineStyle = lineStyle;
        };
        DraggerLine.prototype = new StraightLine([0, 0], [0, 0]);
        DraggerLine.prototype.isHit = function () {
            return false;
        };
        return DraggerLine;
    }
);