/*
 * WebGL core teaching framwork 
 * (C)opyright Hartmut Schirmacher, hschirmacher.beuth-hochschule.de 
 *
 * Module: Band
 *
 * The Band is made of two circles using the specified radius.
 * One circle is at y = height/2 and the other is at y = -height/2.
 *
 */


/* requireJS module definition */
define(["util", "vbo"],
    function (Util, vbo) {

        "use strict";

        /* constructor for Band objects
         * gl:  WebGL context object
         * config: configuration object with the following attributes:
         *         radius: radius of the band in X-Z plane)
         *         height: height of the band in Y
         *         segments: number of linear segments for approximating the shape
         *         asWireframe: whether to draw the band as triangles or wireframe
         *                      (not implemented yet)
         */
        var Band = function (gl, config) {

            // read the configuration parameters
            config = config || {};
            var i, t, x, z, y0, y1,
                radius = config.radius || 1.0,
                height = config.height || 0.1,
                segments = config.segments || 20,
                coords = [],
                lines = [],
                index = 0;
            this.asWireframe = config.asWireframe;

            window.console.log("Creating a " + (this.asWireframe ? "Wireframe " : "") +
                "Band with radius=" + radius + ", height=" + height + ", segments=" + segments);

            // generate vertex coordinates and store in an array
            for(i = 0; i <= segments; i++) {

                // X and Z coordinates are on a circle around the origin
                t = (i / segments) * Math.PI * 2;
                x = Math.sin(t) * radius;
                z = Math.cos(t) * radius;
                // Y coordinates are simply -height/2 and +height/2
                y0 = height / 2;
                y1 = -height / 2;

                // add two points for each position on the circle
                // IMPORTANT: push each float value separately!
                coords.push(x, y0, z);
                coords.push(x, y1, z);

            }

            for(i = segments; --i > 0; index += 2) {
                if(!this.asWireframe) {
                    lines.push(index);
                    lines.push(index + 1);
                    lines.push(index + 3);

                    lines.push(index + 3);
                    lines.push(index + 2);
                    lines.push(index);
                } else {
                    lines.push(index);
                    lines.push(index + 1);

                    lines.push(index);
                    lines.push(index + 2);

                    lines.push(index + 1);
                    lines.push(index + 3);
                }
            }

            if(this.asWireframe) {
                lines.push(index);
                lines.push(index + 1);

                lines.push(index);
                lines.push(index + 2);

                lines.push(index + 1);
                lines.push(index + 3);
            } else {
                lines.push(index);
                lines.push(index + 1);
                lines.push(1);

                lines.push(1);
                lines.push(0);
                lines.push(index);
            }
            // create vertex buffer object (VBO) for the coordinates
            this.coordsBuffer = new vbo.Attribute(gl, { "numComponents" : 3,
                "dataType" : gl.FLOAT,
                "data" : coords
            });

            this.lineBuffer = new vbo.Indices(gl, {
                "indices" : lines
            });

        };

        // draw method: activate buffers and issue WebGL draw() method
        Band.prototype.draw = function (gl, program) {

            // bind the attribute buffers
            this.coordsBuffer.bind(gl, program, "vertexPosition");
            this.lineBuffer.bind(gl);

            // draw the vertices as points
            if(this.asWireframe) {
                gl.drawElements(gl.LINES, this.lineBuffer.numIndices(), gl.UNSIGNED_SHORT, 0);
            } else {
                gl.polygonOffset(2.0, 2.0);
                gl.enable(gl.POLYGON_OFFSET_FILL);
                gl.drawElements(gl.TRIANGLES, this.lineBuffer.numIndices(), gl.UNSIGNED_SHORT, 0);
                gl.disable(gl.POLYGON_OFFSET_FILL);
            }


        };

        // this module only returns the Band constructor function
        return Band;

    }); // define

    
