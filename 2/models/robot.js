define(["util", "vbo", "models/cube", "models/triangle", "models/band", "scene_node", "gl-matrix"],
    function (Util, vbo, Cube, Triangle, Band, SceneNode, glmatrix) {
        "use strict";

        var mat4 = window.mat4,

            Robot = function (gl, programs, config) {
                var cube = new Cube(gl, {silver : true}),
                    band = new Band(gl, {radius : 0.1, height : 1, segments : 20, asWireframe : false}),
                    triangle = new Triangle(gl),

                    torsoSize = [0.6, 1, 0.4],
                    headSize = [torsoSize[0] / 2, torsoSize[0] / 2 + 0.05, torsoSize[2]],
                    eyeSize = [0.1, 0.1, 0],
                    throatSize = [1, headSize[1] / 2, 1],
                    shoulderSize = [0.25, 0.2, 0.25],
                    upperArmSize = [0.8, 0.4, 0.8],
                    elbowSize = [0.17, 0.17, 0.17],
                    lowerArmSize = [0.6, 0.4, 0.6],
                    handSize = [0.2, 0.2, 0.2],

                    throatSkin = new SceneNode("throat skin", [band], programs.red),
                    eyeSkin = new SceneNode("eye skin", [triangle], programs.blue),
                    headSkin = new SceneNode("head skin", [cube], programs.vertexColor),
                    shoulderSkin = new SceneNode("shoulder skin", [cube], programs.vertexColor),
                    torsoSkin = new SceneNode("torso skin", [cube], programs.vertexColor),
                    upperArmSkin = new SceneNode("upperArm skin", [band], programs.gray),
                    elbowSkin = new SceneNode("elbow skin", [cube], programs.vertexColor),
                    lowerArmSkin = new SceneNode("lowerArm skin", [band], programs.gray),
                    handSkin = new SceneNode("hand skin", [triangle], programs.green);

                this.leftEye = new SceneNode("leftEye");
                this.rightEye = new SceneNode("rightEye");
                mat4.translate(this.leftEye.transformation, [-0.07, eyeSize[1] / 2, headSize[2] / 2 + 0.01]);
                mat4.translate(this.rightEye.transformation, [0.07, eyeSize[1] / 2, headSize[2] / 2 + 0.01]);

                this.eyes = new SceneNode("eyes", [this.leftEye, this.rightEye]);
                mat4.scale(eyeSkin.transformation, eyeSize);
                this.leftEye.addObjects([eyeSkin]);
                this.rightEye.addObjects([eyeSkin]);

                this.head = new SceneNode("head", [this.eyes]);
                mat4.rotate(this.head.transformation, 0.2 * Math.PI, [0, 1, 0.05]);
                mat4.translate(this.head.transformation, [0, headSize[1] / 2, 0]);
                mat4.scale(headSkin.transformation, headSize);
                this.head.addObjects([headSkin]);

                this.throat = new SceneNode("throat", [this.head]);
                mat4.translate(this.throat.transformation, [0, torsoSize[1] / 2 + throatSize[1] / 2, 0]);
                mat4.rotate(throatSkin.transformation, 0.6 * Math.PI, [0, 1, 0]);
                mat4.scale(throatSkin.transformation, throatSize);
                this.throat.addObjects([throatSkin]);

                mat4.scale(shoulderSkin.transformation, shoulderSize);
                mat4.scale(upperArmSkin.transformation, upperArmSize);
                mat4.scale(elbowSkin.transformation, elbowSize);
                mat4.scale(lowerArmSkin.transformation, lowerArmSize);
                mat4.scale(handSkin.transformation, handSize);

                // LEFT ARM //

                this.leftHand = new SceneNode("leftHand");
                mat4.translate(this.leftHand.transformation, [0, 0.3, 0]);
                mat4.rotate(this.leftHand.transformation, -0.2 * Math.PI, [0, 1, 0]);
                this.leftHand.addObjects([handSkin]);

                this.leftLowerArm = new SceneNode("leftLowerArm", [this.leftHand]);
                mat4.translate(this.leftLowerArm.transformation, [0, 0, lowerArmSize[1] / 2]);
                mat4.rotate(this.leftLowerArm.transformation, 0.5 * Math.PI, [1, 0, 0]);
                this.leftLowerArm.addObjects([lowerArmSkin]);

                this.leftElbow = new SceneNode("leftElbow", [this.leftLowerArm]);
                mat4.translate(this.leftElbow.transformation, [0, -upperArmSize[1] / 2, 0]);
                this.leftElbow.addObjects([elbowSkin]);

                this.leftUpperArm = new SceneNode("leftUpperArm", [this.leftElbow]);
                mat4.translate(this.leftUpperArm.transformation, [0, -upperArmSize[1] / 2, 0]);
                this.leftUpperArm.addObjects([upperArmSkin]);

                this.leftShoulder = new SceneNode("leftShoulder", [this.leftUpperArm]);
                mat4.translate(this.leftShoulder.transformation, [-torsoSize[0] / 2 - shoulderSize[0] / 2, torsoSize[1] / 2 - shoulderSize[0] / 2, 0]);
                this.leftShoulder.addObjects([shoulderSkin]);

                this.leftArm = new SceneNode("leftArm", [this.leftShoulder]);

                // RIGHT ARM //

                this.rightHand = new SceneNode("rightHand");
                mat4.translate(this.rightHand.transformation, [0, 0.3, 0]);
                mat4.rotate(this.rightHand.transformation, 0.2 * Math.PI, [0, 1, 0]);
                this.rightHand.addObjects([handSkin]);

                this.rightLowerArm = new SceneNode("rightLowerArm", [this.rightHand]);
                mat4.translate(this.rightLowerArm.transformation, [0, 0, lowerArmSize[1] / 2]);
                mat4.rotate(this.rightLowerArm.transformation, 0.5 * Math.PI, [1, 0, 0]);
                this.rightLowerArm.addObjects([lowerArmSkin]);

                this.rightElbow = new SceneNode("rightElbow", [this.rightLowerArm]);
                mat4.translate(this.rightElbow.transformation, [0, -upperArmSize[1] / 2, 0]);
                this.rightElbow.addObjects([elbowSkin]);

                this.rightUpperArm = new SceneNode("rightUpperArm", [this.rightElbow]);
                mat4.translate(this.rightUpperArm.transformation, [0, -upperArmSize[1] / 2, 0]);
                this.rightUpperArm.addObjects([upperArmSkin]);

                this.rightShoulder = new SceneNode("rightShoulder", [this.rightUpperArm]);
                mat4.translate(this.rightShoulder.transformation, [torsoSize[0] / 2 + shoulderSize[0] / 2, torsoSize[1] / 2 - shoulderSize[0] / 2, 0]);
                this.rightShoulder.addObjects([shoulderSkin]);

                this.rightArm = new SceneNode("rightArm", [this.rightShoulder]);

                this.torso = new SceneNode("torso", [this.throat, this.leftArm, this.rightArm]);
                mat4.scale(torsoSkin.transformation, torsoSize);
                this.torso.addObjects([torsoSkin]);
            };

        Robot.prototype.draw = function (gl, program, transformation) {
            this.torso.draw(gl, program, transformation);
        };

        Robot.prototype.rotate = function (rotationAxis, angle) {
            switch(rotationAxis) {
                case "robotHeadY":
                    mat4.rotate(this.head.transformation, angle, [0, 1, 0]);
                    break;
                case "robotHeadX":
                    mat4.rotate(this.head.transformation, angle, [0, -1, 0]);
                    break;
                case "robotHeadUp":
                    mat4.rotate(this.head.transformation, angle, [1, 0, 0]);
                    break;
                case "robotHeadDown":
                    mat4.rotate(this.head.transformation, angle, [-1, 0, 0]);
                    break;
                case "robotLeftShoulderY":
                    mat4.rotate(this.leftShoulder.transformation, angle, [1, 0, 0]);
                    break;
                case "robotLeftShoulderX":
                    mat4.rotate(this.leftShoulder.transformation, angle, [-1, 0, 0]);
                    break;
                case "robotRightShoulderY":
                    mat4.rotate(this.rightShoulder.transformation, angle, [1, 0, 0]);
                    break;
                case "robotRightShoulderX":
                    mat4.rotate(this.rightShoulder.transformation, angle, [-1, 0, 0]);
                    break;
                case "robotLeftElbowY":
                    mat4.rotate(this.leftElbow.transformation, angle, [0, 1, 0]);
                    break;
                case "robotLeftElbowX":
                    mat4.rotate(this.leftElbow.transformation, angle, [0, -1, 0]);
                    break;
                case "robotRightElbowY":
                    mat4.rotate(this.rightElbow.transformation, angle, [0, 1, 0]);
                    break;
                case "robotRightElbowX":
                    mat4.rotate(this.rightElbow.transformation, angle, [0, -1, 0]);
                    break;
                default:
                    window.console.log("BEEP. ROBOT DOES NOT KNOW" + rotationAxis + ". BEEP.");
            }
        };

        return Robot;

    }); // define