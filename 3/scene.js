/*
 *
 * Module scene: Computergrafik 2, Aufgabe 2
 * (C)opyright Hartmut Schirmacher, hschirmacher.beuth-hochschule.de 
 *
 */


/* requireJS module definition */
define(["jquery", "gl-matrix", "util", "program", "shaders", "scene_node",
    "models/parametric", "light", "material", "texture"],
    function ($, glmatrix, util, Program, shader, SceneNode, parametric, light, material, texture) {

        "use strict";

        // simple scene: create some scene objects in the constructor, and
        // draw them in the draw() method
        var Scene = function (gl) {

            // store the WebGL rendering context
            this.gl = gl;

            // object to hold all GPU programs
            this.programs = {};

            // create WebGL program for phong illumination
            this.programs.phong = new Program(gl, shader("phong_vs"),
                shader("phong_fs"));
            this.programs.phong.use();
            this.programs.phong.setUniform("ambientLight", "vec3", [0.4, 0.4, 0.4]);

            this.programs.planet = new Program(gl, shader("planet_vs"),
                shader("planet_fs"));

            // in 3.2 create textures from image files here...

            // in 3.2, bind textures to GPU programs in the following callback func
            var _scene = this, mat4 = window.mat4,
                tex = new texture.Texture2D(gl, "./textures/earth_month04.jpg"),
                nightTex = new texture.Texture2D(gl, "./textures/earth_at_night_2048.jpg"),
                batTex = new texture.Texture2D(gl, "./textures/earth_bathymetry_4096.jpg"),
                cloudTex = new texture.Texture2D(gl, "./textures/earth_clouds_2048.jpg");
            texture.onAllTexturesLoaded(function () {
                _scene.programs.planet.use();
                _scene.programs.planet.setUniform("ambientLight", "vec3", [0.4, 0.4, 0.4]);
                _scene.programs.planet.setTexture("daylightTexture", 0, tex);
                _scene.programs.planet.setTexture("nightTexture", 1, nightTex);
                _scene.programs.planet.setTexture("batTexture", 2, batTex);
                _scene.programs.planet.setTexture("cloudTexture", 3, cloudTex);
                _scene.draw();
            });

            // initial position of the camera
            this.cameraTransformation = mat4.lookAt([0, 0.5, 3], [0, 0, 0], [0, 1, 0]);

            // transformation of the scene, to be changed by animation
            this.worldTransformation = mat4.identity();

            // light source
            this.sun = new light.DirectionalLight("light",
                {"direction" : [-1, 0, 0], "color" : [1, 1, 1] },
                [this.programs.phong, this.programs.planet]);
            this.sunNode = new SceneNode("Sunlight", [this.sun]);

            // equator ring for orientation
            this.ringGeometry = new parametric.Torus(gl, 1.2, 0.04, {"uSegments" : 80, "vSegments" : 40});
            this.ringMaterial = new material.PhongMaterial("material",
                {"ambient" : [0.1, 0.1, 0.2],
                    "diffuse" : [0.2, 0.2, 0.2],
                    "specular" : [0.4, 0.4, 0.4],
                    "shininess" : 80              });
            this.ringNode = new SceneNode("Ring", [this.ringMaterial, this.ringGeometry], this.programs.phong);
            // need to rotate the ring so it is in the X-Z-plane
            mat4.rotate(this.ringNode.transformation, Math.PI / 2, [1, 0, 0]);

            // planet
            this.planetSurface = new parametric.Sphere(gl, 1.0, {"uSegments" : 80, "vSegments" : 80 });
            this.planetMaterial = new material.PhongMaterial("material",
                {"ambient" : [0.05, 0.05, 0.05],
                    "diffuse" : [0.8, 0.2, 0.2],
                    "specular" : [0.4, 0.4, 0.4],
                    "shininess" : 80              });
            this.planetNode = new SceneNode("Planet", [this.planetMaterial, this.planetSurface], this.programs.planet);
            // rotate sphere so the poles are on the Y axis
            mat4.rotate(this.planetNode.transformation, Math.PI / 2, [1, 0, 0]);

            // the root node is our little "universe"
            this.universe = new SceneNode("Universe", [this.sunNode, this.planetNode, this.ringNode], this.programs.phong);

            // the scene has an attribute "drawOptions" that is used by
            // the HtmlController. Each attribute in this.drawOptions
            // automatically generates a corresponding checkbox in the UI.
            this.drawOptions = {
                "Show Planet" : true,
                "Show Ring" : false,
                "Debug" : false,
                "Daytime Texture" : true,
                "Nighttime Texture" : true,
                "RedGreen" : false,
                "Gloss Map" : true,
                "Clouds" : true
            };
        };

        // the scene's draw method draws whatever the scene wants to draw
        Scene.prototype.draw = function () {

            // just a shortcut
            var gl = this.gl, p, mat4 = window.mat4,

            // set up the projection matrix, depending on the canvas size
                aspectRatio = gl.drawingBufferWidth / gl.drawingBufferHeight,
                projection = mat4.perspective(45, aspectRatio, 0.01, 100),

            // multiple world and camera transformations to obtain model-view matrix
                modelViewMatrix = mat4.create(this.cameraTransformation);
            mat4.multiply(modelViewMatrix, this.worldTransformation);

            // set common uniform variables for all used programs
            for(p in this.programs) {
                this.programs[p].use();
                this.programs[p].setUniform("projectionMatrix", "mat4", projection);
            }

            // clear color and depth buffers
            gl.clearColor(0.0, 0.0, 0.0, 1.0);
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

            // set up depth test to discard occluded fragments
            gl.enable(gl.DEPTH_TEST);
            gl.depthFunc(gl.LESS);

            // show/hide certain parts of the scene
            this.ringNode.visible = this.drawOptions["Show Ring"];
            this.planetNode.visible = this.drawOptions["Show Planet"];
            if(this.drawOptions["Debug"]) {
                this.programs.planet.setUniform("debug", "bool", true);
            } else {
                this.programs.planet.setUniform("debug", "bool", false);
            }

            if(this.drawOptions["Daytime Texture"]) {
                this.programs.planet.setUniform("dayTimeTexture", "bool", true);
            } else {
                this.programs.planet.setUniform("dayTimeTexture", "bool", false);
            }

            if(this.drawOptions["Nighttime Texture"]) {
                this.programs.planet.setUniform("nightTimeTexture", "bool", true);
            } else {
                this.programs.planet.setUniform("nightTimeTexture", "bool", false);
            }

            if(this.drawOptions["RedGreen"]) {
                this.programs.planet.setUniform("showBatTexture", "bool", true);
            } else {
                this.programs.planet.setUniform("showBatTexture", "bool", false);
            }

            if(this.drawOptions["Gloss Map"]) {
                this.programs.planet.setUniform("glossMap", "bool", true);
            } else {
                this.programs.planet.setUniform("glossMap", "bool", false);
            }

            if(this.drawOptions["Clouds"]) {
                this.programs.planet.setUniform("showClouds", "bool", true);
            } else {
                this.programs.planet.setUniform("showClouds", "bool", false);
            }

            // draw the scene
            this.universe.draw(gl, this.programs.blue, modelViewMatrix);
        };

        // the scene's rotate method is called from HtmlController, when certain
        // keyboard keys are pressed. Try Y and Shift-Y, for example.
        Scene.prototype.rotate = function (rotationAxis, angle) {

            window.console.log("rotating around " + rotationAxis + " by " + angle + " degrees.");

            // degrees to radians
            angle = angle * Math.PI / 180;

            // manipulate the corresponding matrix, depending on the name of the joint
            switch(rotationAxis) {
                case "worldY":
                    window.mat4.rotate(this.worldTransformation, angle, [0, 1, 0]);
                    break;
                case "worldX":
                    window.mat4.rotate(this.worldTransformation, angle, [1, 0, 0]);
                    break;
                default:
                    window.console.log("axis " + rotationAxis + " not implemented.");
                    break;
            }

            // redraw the scene
            this.draw();
        };

        return Scene;

    }); // define module
        

